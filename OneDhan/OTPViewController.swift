//
//  OTPViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import Foundation

class OTPViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lnlOTP: UILabel!
    @IBOutlet weak var txtOTP: UITextField!
    
    var isFromRegister = false
    var objPhone: String?
    var objOtp: Int?
    
    var objfirstname: String?
    var objlastname: String?
    var objpassword: String?
    var objcompany: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.isHidden = true
        
        txtOTP.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
        String) -> Bool {
        
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnOKVeried(_ sender: Any) {
        self.alertView.isHidden = true
        
        if isFromRegister == true {
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.setupSideMenu()
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassViewController") as! ResetPassViewController
            vc.objPhone = self.objPhone
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func btnSumit(_ sender: Any) {
        if self.isValidatedReset() {
            
            callOTPVeify()
           
        }
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lnlOTP.isHidden = true
        
        if txtOTP.text == "" {
            self.lnlOTP.isHidden = false
            return false
        }
        
        self.lnlOTP.isHidden = true
        
        return true
    }
    
    //MARK:- API Call
    func callOTPVeify() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let param = ["otpsent": objOtp ?? 0, "otprecieved": txtOTP.text!] as [String : Any]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(VALID_OTP, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        
                        if self.isFromRegister == true {
                            // callSignUp()
                             self.callNewSignUp()
                        } else {
                            self.alertView.isHidden = false
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    func callSignUp() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let token = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        
        let param = ["otpsent": "\(objOtp ?? 0)", "otprecieved": txtOTP.text!,"firstname": objfirstname ?? "","lastname": objlastname ?? "","mobile": objPhone ?? "","password": objpassword ?? "","company": objcompany ?? "","device_id": deviceID ?? "","token": token ?? ""] as [String : Any]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("​signup.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        
                        UserDefaults.standard.set(self.objPhone ?? "", forKey: "UserPhone")
                        UserDefaults.standard.set(self.objpassword ?? "", forKey: "UserPassword")
                        UserDefaults.standard.synchronize()

                        self.alertView.isHidden = false
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func callNewSignUp()
    {
        let deviceID = UIDevice.current.identifierForVendor?.uuidString

        var semaphore = DispatchSemaphore (value: 0)
        let firstname = objfirstname ?? ""
        let lastname = objlastname ?? ""
        let Phone = objPhone ?? ""
        let SentOtp = "\(objOtp ?? 0)"
        let company = objcompany ?? ""
 
        let parameters = "firstname=\(firstname)&lastname=\(lastname)&mobile=\(Phone)&password=\(objpassword ?? "")&company=\(company)&otpsent=\(SentOtp)&otprecieved=\(txtOTP.text!)&device_id=\(deviceID ?? "")&token=\(objPhone ?? "")"
        let postData =  parameters.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://onedhan.co/app/dist/api/signup.php")!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("PHPSESSID=d29noph2kp1bkdh08b1ibqlgke", forHTTPHeaderField: "Cookie")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            print(String(data: data, encoding: .utf8)!)
            
            let objDataRes = String(data: data, encoding: .utf8)!
            let jsonData = objDataRes.data(using: .utf8)!
            let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            
            if let personsDictionary = dictionary as? [String: Any] {
                print(personsDictionary)
                if let numbers = personsDictionary["statusCode"] as? Int {
                    print(numbers)
                    
                    if numbers == 200 {
                        
                        DispatchQueue.main.async { [weak self] in
                            self?.alertView.isHidden = false
                        }
                    }
                }
                

            }
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
    }
}
