//
//  HomeViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import Foundation
import MapKit
import Photos
import AssetsLibrary
import StoreKit
import GoogleMobileAds

protocol QRForDelegate
{
    func onQRForDelegateReady(type: String)
}

class HomeViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate,QRForDelegate,GADBannerViewDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var addMobCot: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewSccaneAlert: UIView!
    @IBOutlet weak var lblComanuyName: UILabel!
    
    @IBOutlet weak var lblMsgRespinse: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    var locationManager: CLLocationManager!
    
    var objLatitude: Double?
    var objLongitude: Double?

    var objFullAddrress = String()

    var objCredential: String?
    
    var isScanCompleted = false
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
      
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = 10
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        alertView.isHidden = true
        callFetchvdashboard()
        self.setShadowinHeader(headershadowView: [topView])
        self.setShadowinHeader(headershadowView: [bottomView])
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let imageQR = generateQRCode(from: phonenumber!)
        imgQRCode.image = imageQR

        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)

        addBanner()
        // Do any additional setup after loading the view.
    }

    func addBanner() {
           
           bannerView.delegate = self
           bannerView.rootViewController = self
           bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
           bannerView.load(GADRequest())
           let UDID = UIDevice.current.identifierForVendor?.uuidString

           GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as? [String]
       }
 
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        // Show
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
         print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                //return UIImage(ciImage: output)
                let size = CGSize(width: 1024, height: 1024)
                UIGraphicsBeginImageContextWithOptions(size, false, 0)
                defer { UIGraphicsEndImageContext() }
                UIImage(ciImage: output).draw(in: CGRect(origin: .zero, size: size))
                if let qrcodeImage = UIGraphicsGetImageFromCurrentImageContext() {
                    return qrcodeImage
                   //let imageData = UIImageJPEGRepresentation(qrcodeImage, 1)
                }
            }
        }

        return nil
    }
    
    @IBAction func btnOK(_ sender: Any) {
        self.viewSccaneAlert.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            
            objLatitude = location.coordinate.latitude
            objLongitude = location.coordinate.longitude
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler:
                {
                    placemarks, error -> Void in
                    
                    // Place details
                    guard let placeMark = placemarks?.first else { return }
                    
                    var objLocation = String()
                    var objCity = String()
                    var objCountry = String()
                    // Location name
                    if let locationName = placeMark.location {
                        print(locationName)
                    }
                    // Street address
                    if let street = placeMark.thoroughfare {
                        print(street)
                        objLocation = street
                    }
                    // City
                    if let city = placeMark.subAdministrativeArea {
                        print(city)
                        objCity = city
                    }
                    // Zip code
                    if let zip = placeMark.isoCountryCode {
                        print(zip)
                    }
                    // Country
                    if let country = placeMark.country {
                        print(country)
                        objCountry = country
                    }
                    
                    self.objFullAddrress = "\(objLocation), \(objCity), \(objCountry)"
            })
            
        }
    }
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        alertView.isHidden = true
        callFetchData()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnReport(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCancelLogout(_ sender: Any) {
        alertView.isHidden = true
    }
    
    @IBAction func btnConfilrLogout(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = homeNavigation
        window?.makeKeyAndVisible()

    }
    
    @IBAction func btnScan(_ sender: Any) {
        isScanCompleted = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRScannerController") as! QRScannerController
        vc.delegateQR = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnImageSav(_ sender: Any) {
        
        let currentImage = imgQRCode.image
        UIImageWriteToSavedPhotosAlbum(imgQRCode.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        //MyAwesomeAlbum.shared.save(image: imgQRCode.image!)

    }
   
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error1 = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error1.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your QR Code has been saved to your photos.", preferredStyle: .alert)
            //ac.addAction(UIAlertAction(title: "OK", style: .default))
            ac.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            }))
            present(ac, animated: true)
        }
    }
    
    @IBAction func btnNew(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewVisitorViewController") as! NewVisitorViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .trashDirectory, in: .userDomainMask)
        return paths[0]
    }
    //MARK:- API Call
    func callFetchvdashboard() {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String

        let param = ["mobile": phonenumber ?? ""]
         print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPostArray("mapi/fetchvdashboard.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                 
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
    }
    
    func onQRForDelegateReady(type: String) {
        
        if isScanCompleted == false {
            isScanCompleted = true
            self.objCredential = type
            callAuthenticateQRCode(credential: type)
            callSendPushNotification(credential: type)
        }
    }
    
    func callAuthenticateQRCode(credential: String) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","credential": credential, "latitude": "\(objLatitude ?? 0.0)","longitude": "\(objLongitude ?? 0.0)","location": objFullAddrress]
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/authenticate.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            MBProgressHUD.hide(for: self.view, animated: true)

            if error == nil {

                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let objStatusCode = response?.value(forKey: "statusCode") as? Int
                let message = response?.value(forKey: "message") as? String
                let otpsent = response?.value(forKey: "otpsent") as? Int
                
                if statusCode == 200 {
                    self.viewSccaneAlert.isHidden = false
                    self.lblMsgRespinse.text = message
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.viewSccaneAlert.isHidden = false
                    self.lblMsgRespinse.text = message
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
    }
    
    func callSendPushNotification(credential: String) {
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","credential": credential ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/pushnotification.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                  //  MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let arrResults = response?.value(forKey: "results") as? NSArray
                    let failure = response?.value(forKey: "failure") as? Int
                    
                    if failure == 0 {
                        
                        let message_id = arrResults?.value(forKey: "message_id") as? String
                        
                    } else {
                        let error = arrResults?.value(forKey: "error") as? NSDictionary
                        //self.view.makeToast(error)
                    }
                    
                } else {
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func callFetchData() {
      //  MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/profile-fetch.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        let company = response?.value(forKey: "company") as? String
                        self.lblComanuyName.text = company
                        
                    } else {
                        self.view.makeToast(message)
                    }
 
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }

}
class ImageSaver: NSObject {
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }

    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
    }
}
 
class MyAwesomeAlbum: NSObject {
  static let albumName = "OneDhan"
  static let shared = MyAwesomeAlbum()

  private var assetCollection: PHAssetCollection!

  private override init() {
    super.init()

    if let assetCollection = fetchAssetCollectionForAlbum() {
      self.assetCollection = assetCollection
      return
    }
  }

  private func checkAuthorizationWithHandler(completion: @escaping ((_ success: Bool) -> Void)) {
    if PHPhotoLibrary.authorizationStatus() == .notDetermined {
      PHPhotoLibrary.requestAuthorization({ (status) in
        self.checkAuthorizationWithHandler(completion: completion)
      })
    }
    else if PHPhotoLibrary.authorizationStatus() == .authorized {
      self.createAlbumIfNeeded { (success) in
        if success {
          completion(true)
        } else {
          completion(false)
        }

      }

    }
    else {
      completion(false)
    }
  }

  private func createAlbumIfNeeded(completion: @escaping ((_ success: Bool) -> Void)) {
    if let assetCollection = fetchAssetCollectionForAlbum() {
      // Album already exists
      self.assetCollection = assetCollection
      completion(true)
    } else {
      PHPhotoLibrary.shared().performChanges({
        PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: MyAwesomeAlbum.albumName)   // create an asset collection with the album name
      }) { success, error in
        if success {
          self.assetCollection = self.fetchAssetCollectionForAlbum()
          completion(true)
        } else {
          // Unable to create album
          completion(false)
        }
      }
    }
  }

  private func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
    let fetchOptions = PHFetchOptions()
    fetchOptions.predicate = NSPredicate(format: "title = %@", MyAwesomeAlbum.albumName)
    let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

    if let _: AnyObject = collection.firstObject {
      return collection.firstObject
    }
    return nil
  }

  func save(image: UIImage) {
    self.checkAuthorizationWithHandler { (success) in
      if success, self.assetCollection != nil {
        PHPhotoLibrary.shared().performChanges({
          let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
          let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
          if let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) {
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest.addAssets(enumeration)
          }

        }, completionHandler: { (success, error) in
          if success {
            print("Successfully saved image to Camera Roll.")
          } else {
            print("Error writing to image library: \(error!.localizedDescription)")
          }
        })

      }
    }
  }

  func saveMovieToLibrary(movieURL: URL) {

    self.checkAuthorizationWithHandler { (success) in
      if success, self.assetCollection != nil {

        PHPhotoLibrary.shared().performChanges({

          if let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: movieURL) {
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            if let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) {
              let enumeration: NSArray = [assetPlaceHolder!]
              albumChangeRequest.addAssets(enumeration)
            }

          }

        }, completionHandler:  { (success, error) in
          if success {
            print("Successfully saved video to Camera Roll.")
          } else {
            print("Error writing to movie library: \(error!.localizedDescription)")
          }
        })


      }
    }

  }
}
