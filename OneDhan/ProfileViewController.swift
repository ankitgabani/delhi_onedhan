//
//  ProfileViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import GoogleMobileAds

class ProfileViewController: UIViewController,UITextFieldDelegate,GADBannerViewDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var HideCont: NSLayoutConstraint!
    
    @IBOutlet weak var alertViw: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var lblErrorFirstName: UILabel!
    
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var lblLastName: UILabel!
    
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var lblCompanmy: UILabel!
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    
    
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var txtDis: UITextField!
    @IBOutlet weak var lblDis: UILabel!
    
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    
    var isFromHome = false
    
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtCompany.delegate = self
        txtState.delegate = self
        txtDis.delegate = self
        txtCity.delegate = self
        alertViw.isHidden = true
        self.setShadowinHeader(headershadowView: [mainView])
        callFetchData()
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)
        
        addBanner()
        // Do any additional setup after loading the view.
    }
    
    func addBanner() {
        
        bannerView.delegate = self
        bannerView.rootViewController = self
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.load(GADRequest())
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as? [String]
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        
        // Show
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.setupSideMenu()
    }
    
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            txtLastName.becomeFirstResponder()
            return false
        } else if textField == txtLastName {
            txtCompany.becomeFirstResponder()
            return false
        } else if textField == txtCompany {
            txtState.becomeFirstResponder()
            return false
        } else if textField == txtState {
            txtDis.becomeFirstResponder()
            return false
        } else if textField == txtDis {
            txtCity.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertViw.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        alertViw.isHidden = true
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnCancleLog(_ sender: Any) {
        alertViw.isHidden = true
    }
    
    @IBAction func btnConfirmLog(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = homeNavigation
        window?.makeKeyAndVisible()
    }
    
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    @IBAction func btnSaveChange(_ sender: Any) {
        
        if self.isValidatedReset() {
            callUpdateProfile()
        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.setupSideMenu()
        
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lblErrorFirstName.isHidden = true
        self.lblLastName.isHidden = true
        self.lblCompanmy.isHidden = true
        self.lblState.isHidden = true
        self.lblDis.isHidden = true
        self.lblCity.isHidden = true
        
        if txtFirstName.text == "" {
            self.lblErrorFirstName.isHidden = false
            return false
        } else if txtLastName.text == "" {
            self.lblLastName.isHidden = false
            return false
        } else if txtCompany.text == "" {
            self.lblCompanmy.isHidden = false
            return false
        } else if txtState.text == "" {
            self.lblState.isHidden = false
            return false
        } else if txtDis.text == "" {
            self.lblDis.isHidden = false
            return false
        } else if txtCity.text == "" {
            self.lblCity.isHidden = false
            return false
        }
        
        self.lblErrorFirstName.isHidden = true
        self.lblLastName.isHidden = true
        self.lblCompanmy.isHidden = true
        self.lblState.isHidden = true
        self.lblDis.isHidden = true
        self.lblCity.isHidden = true
        
        return true
    }
    
    //MARK:- API Call
    func callFetchData() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/profile-fetch.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        let city = response?.value(forKey: "city") as? String
                        let company = response?.value(forKey: "company") as? String
                        let district = response?.value(forKey: "district") as? String
                        let firstname = response?.value(forKey: "firstname") as? String
                        let lastname = response?.value(forKey: "lastname") as? String
                        let mobile = response?.value(forKey: "mobile") as? String
                        let state = response?.value(forKey: "state") as? String
                        
                        self.txtFirstName.text = firstname ?? ""
                        self.txtCity.text = city ?? ""
                        self.txtDis.text = district ?? ""
                        self.txtLastName.text = lastname ?? ""
                        self.txtMobileNumber.text = mobile ?? ""
                        self.txtState.text = state ?? ""
                        self.txtCompany.text = company ?? ""
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func callUpdateProfile() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","firstname": txtFirstName.text!,"lastname": txtLastName.text!,"company": txtCompany.text!,"state": txtState.text!,"city": txtCity.text!,"district": txtDis.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/modify-signup.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        self.view.makeToast(message)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
}


//url = http://onedhan.co/app/dist/api/mapi/profile-fetch.php
//["mobile": "7096859504"]
//STATUS CODE Optional(200)
//Response Optional({
//    city = city;
//    company = surat;
//    district = Tssg;
//    firstname = Ankit;
//    lastname = Patel;
//    message = "user found!";
//    mobile = 7096859504;
//    state = state;
//    statusCode = 200;
//})
