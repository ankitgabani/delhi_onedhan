//
//  LoginViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import LGSideMenuController
import Toast_Swift

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var lnlErrorPhone: UILabel!
    @IBOutlet weak var lblErrorPass: UILabel!
    
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var imgRemember: UIImageView!
    
    var clickRemember : Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        
        txtPhone.delegate = self
        txtPassword.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setUp() {
        
        if let isRemember = UserDefaults.standard.value(forKey: "isRemember") as? Bool {

            if isRemember == true {
                 imgRemember.image = UIImage(named: "ic_check")
                let email = UserDefaults.standard.value(forKey: "UserPhone") as? String
                self.txtPhone.text = email
                let password = UserDefaults.standard.value(forKey: "UserPassword") as? String
                self.txtPassword.text = password
                clickRemember = true
            }

        } else {
            imgRemember.image = UIImage(named: "ic_uncheck")
            clickRemember = false
            self.txtPhone.text! = ""
            self.txtPassword.text! = ""
        }
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtPhone {
            txtPassword.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
        String) -> Bool {
        
        if textField == txtPhone {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    @IBAction func btnRemeberMe(_ sender: Any) {
        if(clickRemember == true) {
            imgRemember.image = UIImage(named: "ic_uncheck")
            UserDefaults.standard.set(false, forKey: "isRemember")
            UserDefaults.standard.synchronize()
            clickRemember = false
        } else {
            imgRemember.image = UIImage(named: "ic_check")
            UserDefaults.standard.set(true, forKey: "isRemember")
            UserDefaults.standard.synchronize()
            clickRemember = true
        }
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lblErrorPass.isHidden = true
        self.lnlErrorPhone.isHidden = true
        
        
        if txtPhone.text == "" {
            self.lnlErrorPhone.isHidden = false
            return false
        } else if txtPassword.text == "" {
            self.lblErrorPass.isHidden = false
            return false
        }
        
        self.lblErrorPass.isHidden = true
        self.lnlErrorPhone.isHidden = true
        
        return true
    }
    
    @IBAction func btnSignIn(_ sender: Any) {
        
        if self.isValidatedReset() {
            callLogin()
        }
        
    }
    
    
    
    //MARK:- API Call
    func callLogin() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        

        let token = UserDefaults.standard.value(forKey: "FirebaseToken") as? String
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        
        let param = ["username": txtPhone.text!,"password1": txtPassword.text!,"device_id": deviceID ?? "","token": token ?? ""]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(USER_LOGIN, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.set(self.txtPhone.text!, forKey: "UserPhone")
                        UserDefaults.standard.set(self.txtPassword.text!, forKey: "UserPassword")
                        UserDefaults.standard.synchronize()
                        
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.setupSideMenu()
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
}


extension UINavigationController
{
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}
