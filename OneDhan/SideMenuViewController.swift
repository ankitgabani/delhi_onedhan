//
//  SideMenuViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import LGSideMenuController
import StoreKit
extension Notification.Name {
    
    public static let myNotificationKeyLogout = Notification.Name(rawValue: "myNotificationKeyLogout")
}


class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var lblDaiuky: UILabel!
    
    @IBOutlet weak var imgConant: NSLayoutConstraint!
    //30
    
    @IBOutlet weak var btnDailyConty: NSLayoutConstraint!
    //55
    
    var isOpenReprt = false
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDailyConty.constant = 0
        imgConant.constant = 0
        lblDaiuky.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnHome(_ sender: Any) {
        
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let controller = window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigation.viewControllers = [vc]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let controller = window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigation.viewControllers = [vc]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func btnNewVisitor(_ sender: Any) {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let controller = window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewVisitorViewController") as! NewVisitorViewController
        navigation.viewControllers = [vc]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func btnReport(_ sender: Any) {
        
        if isOpenReprt == true {
            isOpenReprt = false
            btnDailyConty.constant = 0
            imgConant.constant = 0
            lblDaiuky.isHidden = true

        } else {
            isOpenReprt = true
            btnDailyConty.constant = 55
            imgConant.constant = 30
            lblDaiuky.isHidden = false

        }
    }
    
    @IBAction func btnDailyReport(_ sender: Any) {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        let controller = window?.rootViewController as! LGSideMenuController
        let navigation = controller.rootViewController as! UINavigationController

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        navigation.viewControllers = [vc]
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func btnRate(_ sender: Any) {
        AppSupport.rateApp()
    }
    
    @IBAction func btnShare(_ sender: Any) {
        AppSupport.shareApp(inController: self)
    }
    
    @IBAction func btnLogout(_ sender: Any) {

          NotificationCenter.default.post(name: Notification.Name.myNotificationKeyLogout, object: nil, userInfo:["text": "123"]) // Notification
        
        self.sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
    }
}

struct AppSupport {
    
    static let appID = "1481807442"
    
    static func rateApp(){
         if #available(iOS 10.3, *) {
               SKStoreReviewController.requestReview()

           } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + appID) {
               if #available(iOS 10, *) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)

               } else {
                   UIApplication.shared.openURL(url)
               }
           }
    }
    
    static func openURL(_ url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static let shareText = "OneDhan - "
    
    static func shareApp(inController controller:UIViewController){
        let textToShare = "\(AppSupport.shareText) \n itms-apps://itunes.apple.com/app/\(appID)"
        AppSupport.itemShare(inController: controller, items: textToShare)
    }
    
    static func itemShare(inController controller:UIViewController, items:Any){
        let objectsToShare = [items]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = controller.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 100, y: 200, width: 300, height: 300)
        controller.present(activityVC, animated: true, completion: nil)
    }
    
}
