//
//  SlashViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD

class SlashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        perform(#selector(startAnimLogo), with: nil, afterDelay: 2.0)
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func startAnimLogo() {
        
        if let userLoggedIn = UserDefaults.standard.value(forKey: "isUserLoggedIn") as? Bool
        {
            if userLoggedIn == true {
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.setupSideMenu()
                
            } else {
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(home, animated: true)
            }
        } else {
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(home, animated: true)
        }
        
        
    }
    
}
