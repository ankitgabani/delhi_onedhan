//
//  ResetPassViewController.swift
//  OneDhan
//
//  Created by Ankit on 02/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import MBProgressHUD

class ResetPassViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var txtNewPass: UITextField!
    
    @IBOutlet weak var txtConfirm: UITextField!
    
    var objPhone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNewPass.delegate = self
        txtConfirm.delegate = self
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TextField Delegate Methods
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         
         if textField == txtNewPass {
             txtConfirm.becomeFirstResponder()
             return false
         }
         textField.resignFirstResponder()
         return true
     }
    
     
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
         String) -> Bool {
        
         return true
     }
    
    @IBAction func btnOKSuccess(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: LoginViewController.self) {
                    isExist = true
                    break
                }
            }
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: ForgotPasswordViewController.self) {
                    isExist = true
                    break
                }
            }
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if self.isValidatedLogin() {
            callLogin()
        }
    }
    
    // MARK:- Validation
    func isValidatedLogin() -> Bool {
        
        if txtNewPass.text == "" {
            self.view.makeToast("Please enter new password")
            return false
            
        } else if txtConfirm.text == "" {
            self.view.makeToast("Please enter confirm password")
            return false
            
        }  else if txtNewPass.text != txtConfirm.text {
            self.view.makeToast("Confirm password not match.")
            return false
        }
        return true
    }
    
    //MARK:- API Call
    func callLogin() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let param = ["mobile": "\(objPhone ?? "")","password1": txtNewPass.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/reset-password.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if objStatusCode == 200 {
                        //self.view.makeToast(message)
                        
                        self.alertView.isHidden = false
                        
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
}
