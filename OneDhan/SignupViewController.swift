//
//  SignupViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignupViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtCompantName: UITextField!
    
    @IBOutlet weak var imgRember: UIImageView!
    
    @IBOutlet weak var lblErrorFirst: UILabel!
    @IBOutlet weak var lblErrorLast: UILabel!
    @IBOutlet weak var lblErrorPhone: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblErrorCompny: UILabel!
    
    @IBOutlet weak var lblErrorVali: UILabel!
    
    var clickAgree : Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        clickAgree = false
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtPhone.delegate = self
        txtPassword.delegate = self
        txtCompantName.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
           String) -> Bool {
           
           if textField == txtPhone {
               let maxLength = 10
               let currentString: NSString = textField.text! as NSString
               let newString: NSString =
                   currentString.replacingCharacters(in: range, with: string) as NSString
               return newString.length <= maxLength
           }
           
           return true
       }
    
    // MARK: - TextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtFirstName {
            txtLastName.becomeFirstResponder()
            return false
        } else if textField == txtLastName {
            txtPhone.becomeFirstResponder()
            return false
        } else if textField == txtPhone {
            txtPassword.becomeFirstResponder()
            return false
        } else if textField == txtPassword {
            txtCompantName.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lblErrorFirst.isHidden = true
        self.lblErrorLast.isHidden = true
        self.lblErrorPhone.isHidden = true
        self.lblPassword.isHidden = true
        self.lblErrorCompny.isHidden = true
        self.lblErrorVali.isHidden = true
        
        if txtFirstName.text == "" {
            self.lblErrorFirst.isHidden = false
            return false
        } else if txtLastName.text == "" {
            self.lblErrorLast.isHidden = false
            return false
        } else if txtPhone.text == "" {
            self.lblErrorPhone.isHidden = false
            return false
        } else if txtPhone.text!.count < 10 {
            self.lblErrorPhone.isHidden = false
            return false
        } else if txtPassword.text == "" {
            self.lblPassword.isHidden = false
            return false
        } else if txtPassword.text!.count < 5 {
            self.lblErrorVali.isHidden = false
            return false
        } else if txtCompantName.text == "" {
            self.lblErrorCompny.isHidden = false
            return false
        }   else if clickAgree == false {
            self.view.makeToast("Please accept terms & condition")
            return false
        }
        
        self.lblErrorFirst.isHidden = true
        self.lblErrorLast.isHidden = true
        self.lblErrorPhone.isHidden = true
        self.lblPassword.isHidden = true
        self.lblErrorCompny.isHidden = true
        self.lblErrorVali.isHidden = true
        
        return true
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        if self.isValidatedReset() {
            callSignUP()
        }
    }
    
    @IBAction func btnAgree(_ sender: Any) {
        if(clickAgree == true) {
            imgRember.image = UIImage(named: "ic_uncheck")
            UserDefaults.standard.set(false, forKey: "isRemember")
            UserDefaults.standard.synchronize()
            clickAgree = false
        } else {
            imgRember.image = UIImage(named: "ic_check")
            UserDefaults.standard.set(true, forKey: "isRemember")
            UserDefaults.standard.synchronize()
            clickAgree = true
        }
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTers(_ sender: Any) {
    }
    
    
    //MARK:- API Call
    func callSignUP() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        
        let param = ["mobile": txtPhone.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(OTP_SENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    let otpsent = response?.value(forKey: "otpsent") as? Int
                    
                    if objStatusCode == 200 {
                        
                       
                       let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        vc.objPhone = self.txtPhone.text!
                        vc.objOtp = otpsent
                        vc.isFromRegister = true
                        vc.objfirstname = self.txtFirstName.text!
                        vc.objcompany = self.txtCompantName.text!
                        vc.objlastname = self.txtLastName.text!
                        vc.objpassword = self.txtPassword.text!
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
}
