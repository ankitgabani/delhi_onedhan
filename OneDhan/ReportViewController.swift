//
//  ReportViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import Foundation
import GoogleMobileAds
class ReportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,GADBannerViewDelegate {
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var mainmaView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var txtEndDate: UITextField!
    
    @IBOutlet weak var txtStartDate: UITextField!
    
    @IBOutlet weak var HideViewCont: NSLayoutConstraint!
    //77
    
    @IBOutlet weak var hideContStart: NSLayoutConstraint!
    // 30
    
    @IBOutlet weak var hideContEnd: NSLayoutConstraint!
    // 30
    
    @IBOutlet weak var lblHideClear: UILabel!
    
    var isSearching: Bool = false
    
    var isSelectDate: Bool = false
    let datePicker = UIDatePicker()
    
    let datePicker1 = UIDatePicker()
    var arrVisitorList = [VisitReportList]()
    var arrVisitorListSearching = [VisitReportList]()
    
    var objschedule_start_date: String?
    var objschedule_end_date: String?
    var isLogout = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HideViewCont.constant = 0
        hideContStart.constant = 0
        hideContEnd.constant = 0
        lblHideClear.isHidden = true
        alertView.isHidden = true
        searchBar.delegate = self
        self.setShadowinHeader(headershadowView: [mainmaView])
        
        tblView.delegate = self
        tblView.dataSource = self
        
        searchBar.addTarget(self, action: #selector(ReportViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        
        callGetVisitorReports()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationLogout(notification:)), name: Notification.Name.myNotificationKeyLogout, object: nil)
        
        addBanner()
        // Do any additional setup after loading the view.
    }
    
    func addBanner() {
        
        bannerView.delegate = self
        bannerView.rootViewController = self
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.load(GADRequest())
        let UDID = UIDevice.current.identifierForVendor?.uuidString
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
            [UDID] as? [String]
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
       
        // Show
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    @IBAction func btnHome(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.setupSideMenu()
    }
    
    @objc func onNotificationLogout(notification:Notification)
    {
        alertView.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        alertView.isHidden = true
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if self.searchBar.text!.isEmpty {
            
            self.isSearching = false
            
            self.tblView.reloadData()
            
        } else {
            self.isSearching = true
            
            self.arrVisitorListSearching.removeAll(keepingCapacity: false)
            
            for i in 0..<self.arrVisitorList.count {
                
                let listItem: VisitReportList = self.arrVisitorList[i]
                if listItem.VISITOR_MOBILE_NUMBER!.lowercased().range(of: self.searchBar.text!.lowercased()) != nil {
                    self.arrVisitorListSearching.append(listItem)
                }
            }
            
            self.tblView.reloadData()
        }
        
    }
    
    @IBAction func btnExportPDF(_ sender: Any) {
        callExportPDF()
    }
    @IBAction func btnCancelLogout(_ sender: Any) {
        alertView.isHidden = true
    }
    
    @IBAction func btnConfirmLogt(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.synchronize()
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.rootViewController = homeNavigation
        window?.makeKeyAndVisible()
    }
    
    @IBAction func clickedStartDate(_ sender: UITextField) {
        
        datePicker.datePickerMode = .dateAndTime
        datePicker.maximumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        sender.inputAccessoryView = toolbar
        sender.inputView = datePicker
        
    }
    
    func isValidatedReset() -> Bool {
        if txtStartDate.text == "" {
            self.view.makeToast("Please select Start Date first!")
            return false
        }
        return true
    }
    
    @objc func donedatePicker(){
        //For date formate
        objschedule_end_date = ""
        txtEndDate.text = ""
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objschedule_start_date = formatter1.string(from: datePicker.date)
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        txtStartDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @IBAction func clickedEndDate(_ sender: UITextField) {
        
        if self.isValidatedReset() {
            
            datePicker1.datePickerMode = .dateAndTime
            
            datePicker1.minimumDate = Date()
            
            //ToolBar
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker1));
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker1));
            toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
            sender.inputAccessoryView = toolbar
            sender.inputView = datePicker1
        } else {
            self.view.endEditing(true)
        }
        
    }
    
    @objc func donedatePicker1(){
        //For date formate
        isSelectDate = true
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        objschedule_end_date = formatter1.string(from: datePicker1.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        txtEndDate.text = formatter.string(from: datePicker1.date)
        self.view.endEditing(true)
        
        CallTest()
        // callGetVisitorReportsFilters()
    }
    
    @objc func cancelDatePicker1() {
        self.view.endEditing(true)
    }
    
    @IBAction func clickedStartFiltering(_ sender: Any) {
        HideViewCont.constant = 77
        hideContStart.constant = 30
        hideContEnd.constant = 30
        lblHideClear.isHidden = false
    }
    
    @IBAction func clickedCrealFiltered(_ sender: Any) {
        HideViewCont.constant = 0
        hideContStart.constant = 0
        hideContEnd.constant = 0
        lblHideClear.isHidden = true
        
        callGetVisitorReports()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isSearching == true {
            return arrVisitorListSearching.count
        } else {
            return arrVisitorList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "VisitorNameCell") as! VisitorNameCell
        
        var objVisitorList: VisitReportList!
        
        if self.isSearching == true {
            objVisitorList = self.arrVisitorListSearching[indexPath.row]
        } else {
            objVisitorList = self.arrVisitorList[indexPath.row]
        }
        
        let name = objVisitorList.VISITOR_NAME ?? ""
        let phone = objVisitorList.VISITOR_MOBILE_NUMBER ?? ""
        let location = objVisitorList.Location ?? ""
        let DATETIME = objVisitorList.DATETIME ?? ""
        let imgURL = objVisitorList.qrcode_path ?? ""
        
        cell.lblName.text = name
        cell.lblPhone.text = phone
        cell.lblAddress.text = location
        cell.lblDatye.text = DATETIME
        
        cell.btnPhoneCall.tag = indexPath.row
        cell.btnPhoneCall.addTarget(self, action: #selector(clickedPhoneCall(_:)), for: .touchUpInside)
        
        if location == "" {
            cell.imgLocationCont.constant = 0
        } else {
            cell.imgLocationCont.constant = 22
        }
        
        if imgURL == "" {
            cell.imgChaCont.constant = 0
        } else {
            cell.imgChaCont.constant = 70
        }
        
        let url = URL(string: "http://onedhan.co/app/dist/api/\(imgURL)")
        cell.imgVisit.sd_setImage(with: url, placeholderImage: nil)
        
        self.setShadowinHeader(headershadowView: [cell.mainView])
        return cell
    }
    
    @objc func clickedPhoneCall(_ sender: AnyObject) {
        
        let phoneCall = self.arrVisitorList[sender.tag]
        
        if let url = URL(string: "tel://\(phoneCall.VISITOR_MOBILE_NUMBER ?? "")"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func setShadowinHeader(headershadowView: [UIView]? = nil, shadowBtn: [UIButton]? = nil, setRound: Bool = false) {
        // Shadow and Radius
        if headershadowView != nil {
            for i in headershadowView! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
        
        if shadowBtn != nil {
            for i in shadowBtn! {
                i.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
                i.layer.shadowOffset = CGSize(width: 0, height: 3)
                i.layer.shadowOpacity = 2
                i.layer.shadowRadius = 3
                i.layer.masksToBounds = false
                if setRound == true {
                    i.layer.cornerRadius = i.frame.height/2
                }
            }
        }
    }
    
    func callGetVisitorReports() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? ""]
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPostVisitorReports("​​http://onedhan.co/app/dist/api/mapi/fetch-visitor.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    let list = response?["data"] as? NSArray
                    self.arrVisitorList.removeAll()
                    for FAQList in list! {
                        let list = VisitReportList(ClubsListDictionary: FAQList as? NSDictionary)
                        self.arrVisitorList.append(list)
                    }
                    
                    self.tblView.reloadData()
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func callGetVisitorReportsFilters() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        
        let param = ["mobile": phonenumber ?? "","begindate": "\(objschedule_start_date ?? "") 00:00:00","enddate": "\(objschedule_end_date ?? "") 23:59:59​"]
        print(param)
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("​​mapi/fetchvisitordate.php", parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    let list = response?["data"] as? NSArray
                    
                    for FAQList in list! {
                        let list = VisitReportList(ClubsListDictionary: FAQList as? NSDictionary)
                        self.arrVisitorList.append(list)
                    }
                    
                    self.tblView.reloadData()
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
    func CallTest() {
        
        DispatchQueue.main.async { [weak self] in
            MBProgressHUD.showAdded(to: self!.view, animated: true)
        }
        
        var semaphore = DispatchSemaphore (value: 0)
        
        let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
        let begindate = "\(objschedule_start_date ?? "") 00:00:00"
        let enddate = "\(objschedule_end_date ?? "") 23:59:59​"
        
        let parameters = "mobile=\(phonenumber ?? "")&begindate=\(begindate)&enddate=\(enddate)"
        let postData =  parameters.data(using: .utf8)
        
        var request = URLRequest(url: URL(string: "http://onedhan.co/app/dist/api/mapi/fetchvisitordate.php")!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("PHPSESSID=d29noph2kp1bkdh08b1ibqlgke", forHTTPHeaderField: "Cookie")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        DispatchQueue.main.async { [weak self] in
            MBProgressHUD.hide(for: self!.view, animated: true)
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                
                print(String(describing: error))
                return
            }
            //  print(String(data: data, encoding: .utf8)!)
            
            let objDataRes = String(data: data, encoding: .utf8)!
            
            let jsonData = objDataRes.data(using: .utf8)!
            let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
            
            if let personsDictionary = dictionary as? [String: Any] {
                print(personsDictionary)
                if let numbers = personsDictionary["data"] as? NSArray {
                    print(numbers)
                    
                    self.arrVisitorList.removeAll()
                    for FAQList in numbers {
                        
                        print(FAQList)
                        let list = VisitReportList(ClubsListDictionary: FAQList as? NSDictionary)
                        
                        print(list)
                        self.arrVisitorList.append(list)
                    }
                    
                    print(self.arrVisitorList)
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.tblView.reloadData()
                    }
                    
                    
                }
            }
            
            
            // self.tblView.reloadData()
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        
    }
    
    //MARK:- API Call
    func callExportPDF() {
        
        
        if isSelectDate == true {
            
            let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
            let begindate = "'\(objschedule_start_date ?? "") 00:00:00'"
            let enddate = "'\(objschedule_end_date ?? "") 23:59:59​'"
            
            var strImage = "http://onedhan.co/app/dist/api/mapi/pdfexportdate.php?mobile=\(phonenumber ?? "")&begindate=\(begindate)&enddate=\(enddate)"
            strImage = strImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let urlRE = URL(string: strImage) {
                UIApplication.shared.open(urlRE)
            }
            
        } else {
            
            let phonenumber = UserDefaults.standard.value(forKey: "UserPhone") as? String
            
            if let url = URL(string: "http://onedhan.co/app/dist/api/mapi/pdfexport.php?mobile=\(phonenumber ?? "")") {
                UIApplication.shared.open(url)
            }
        }
        
    }
}

class FileDownloader {
    
    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }
    
    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
}
