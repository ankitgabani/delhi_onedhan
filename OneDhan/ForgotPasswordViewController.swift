//
//  ForgotPasswordViewController.swift
//  OneDhan
//
//  Created by Ankit on 01/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import UIKit
import MBProgressHUD
import MBProgressHUD

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var lnlPhone: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnOTP(_ sender: Any) {
        
        if self.isValidatedReset() {
            callPhoneVerify()
        }
    }
    
    @IBAction func btnBakck(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            
            let viewcontrollers = self.navigationController?.viewControllers
            var isExist = false
            for viewcontroller in viewcontrollers! {
                if viewcontroller.isKind(of: LoginViewController.self) {
                    isExist = true
                    break
                }
            }
            self.navigationController?.viewControllers.insert(controller, at: (viewcontrollers?.count)!)
            self.navigationController?.popToViewController(controller, animated: true)
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string:
        String) -> Bool {
        
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
       
    }
    
    // MARK:- Validation
    func isValidatedReset() -> Bool {
        self.lnlPhone.isHidden = true
        
        if txtPhone.text == "" {
            self.lnlPhone.isHidden = false
            return false
        }
        
        self.lnlPhone.isHidden = true
        
        return true
    }
    
    //MARK:- API Call
    func callPhoneVerify() {
          MBProgressHUD.showAdded(to: self.view, animated: true)

          let param = ["mobile": txtPhone.text!]
          print(param)
          
          APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost("mapi/checkmobile.php", parameters: param, completionHandler: { (response, error, statusCode) in
              
              if error == nil {
                  print("STATUS CODE \(String(describing: statusCode))")
                  print("Response \(String(describing: response))")
                  
                  if statusCode == 200 {
                      MBProgressHUD.hide(for: self.view, animated: true)
                      
                      let objStatusCode = response?.value(forKey: "statusCode") as? Int
                      let message = response?.value(forKey: "message") as? String
                      let otpsent = response?.value(forKey: "otpsent") as? Int
                      
                      if objStatusCode == 200 {
                        self.callLogin()

                      } else {
                          self.view.makeToast(message)
                      }
                      
                      
                  } else {
                      MBProgressHUD.hide(for: self.view, animated: true)
                  }
                  
              } else {
                  MBProgressHUD.hide(for: self.view, animated: true)
                  print("Response \(String(describing: response))")
              }
          })
          
      }
    
    func callLogin() {
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let param = ["mobile": txtPhone.text!]
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithoutAuthHeaderPost(OTP_SENT, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                if statusCode == 200 {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    let objStatusCode = response?.value(forKey: "statusCode") as? Int
                    let message = response?.value(forKey: "message") as? String
                    let otpsent = response?.value(forKey: "otpsent") as? Int
                    
                    if objStatusCode == 200 {
                         let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        vc.objPhone = self.txtPhone.text!
                        vc.objOtp = otpsent
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                    
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Response \(String(describing: response))")
            }
        })
        
    }
    
}
