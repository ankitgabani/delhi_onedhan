//
//  VisitReportList.swift
//  OneDhan
//
//  Created by Ankit on 02/08/20.
//  Copyright © 2020 Gabani Solution. All rights reserved.
//

import Foundation

class VisitReportList: NSObject {
    
    var DATETIME: String?
    var Location: String?
    var VEID: String?
    var VISITOR_MOBILE_NUMBER: String?
    var VISITOR_NAME: String?
    var qrcode_path: String?
    
    
    override init() {
        super.init()
    }
    
    init(ClubsListDictionary: NSDictionary?) {
        super.init()
            
        DATETIME = ClubsListDictionary?.value(forKey: "DATETIME") as? String
        Location = ClubsListDictionary?.value(forKey: "Location") as? String
        VEID = ClubsListDictionary?.value(forKey: "VEID") as? String
        VISITOR_MOBILE_NUMBER = ClubsListDictionary?.value(forKey: "VISITOR_MOBILE_NUMBER") as? String
        VISITOR_NAME = ClubsListDictionary?.value(forKey: "VISITOR_NAME") as? String
        qrcode_path = ClubsListDictionary?.value(forKey: "qrcode_path") as? String
    }
}
